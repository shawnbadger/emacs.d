;; Do not edit!  This file is generated.

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(initial-frame-alist '((fullscreen . maximized)))
 '(load-dirs t)
 '(lsp-ui-peek-always-show t nil nil "Customized with use-package lsp-ui")
 '(lsp-ui-peek-peek-height 30)
 '(package-selected-packages
   '(ztree cmake-mode evil-org evil-org-mode org org-mode evil-magit spaceline-all-the-icons spacemacs-dark-theme spacemacs-theme doom-themes company-box bug-hunter lsp-rust lsp-ui-flycheck hasky-stack haskell-mode evil-collection racket-mode ag smooth-scrolling simpleclip meson-mode buffer-move company-lsp counsel-projectile which-key use-package monokai-theme load-dir helm-projectile general evil-escape counsel ample-zen-theme ample-theme))
 '(safe-local-variable-values '((helm-make-build-dir . "build/"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ivy-virtual ((t (:inherit font-lock-builtin-face :foreground "gray20"))))
 '(lsp-ui-doc-background ((t (:background "black")))))
