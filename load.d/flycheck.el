;; flycheck.el

(use-package flycheck
  :ensure t
  :init
  (setq flycheck-error-list-minimum-level 'warning))
