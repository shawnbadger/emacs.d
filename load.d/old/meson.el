;; meson.el

(use-package meson-mode
  :config
  (add-hook 'meson-mode-hook 'company-mode))
