;; org.el

(use-package org
  :ensure t)

(use-package evil-org
  :ensure t
  :after org)
